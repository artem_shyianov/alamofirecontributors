
#Introduction

This application shows list of Alamofire Contrubutors from Github.

## Installation
```bash
$ git clone https://artem_shyianov@bitbucket.org/artem_shyianov/alamofirecontributors.git
$ cd AlamofireContributors/
$ pod update
$ open AlamofireContributors.xcworkspace 
```

## CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```
